# MCSalt

Estimating Model Confidence Sets (MCS) in R - an alternative implementation.

## About

This repository offers an R implementation of the algorithms described by Hansen,
Lunde and Nason (2011) in their Econometrica paper to estimate so-called 'model
confidence sets'. These are sets of models that contain all 'best' models with a
certain probability. The notion of 'confidence set' is particularly useful in
situations where different or competing model specifications are available and it is uncertain which model is (or might be) appropriate. 

The algorithms come in two flavours, an in-sample and an out-of-sample version. Both of these are implemented here.

There is another implementation of these algorithms available on CRAN in the package [MCS](https://cran.r-project.org/web/packages/MCS/index.html). Maybe some
of the code from here can be merged to over there or results from the two packages
could be compared for correctness and efficiency.

## Getting Started



## Installation 

TODO check
`devtools::install_github("nielsaka/mcsalt.git")`

Locally, the package can be installed in the terminal via `R CMD INSTALL --preclean /path/to/MCSalt`.

## Prerequisites

## Usage

[//]: # ## Contributing

[//]: # ## History

[//]: # ## Credits

## License

This project is licensed under the MIT License. See the LICENSE file for details.


