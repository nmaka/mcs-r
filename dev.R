setwd("~/proj/mcs/mcs-r")
needed.pkgs <- c("devtools", "roxygen2")
lapply(needed.pkgs, require, character.only = T)
options(devtools.install.args = "--preclean") # remove previous .dll ...


#### Documentation Workflow ####
# switch to development mode: sandbox
dev_mode()
# load all fn, data, doc of development version
load_all()
# edit commentary/doc and update Rd files
document(, , "rd")
?estMCS
# check new help files
check_doc()
# do complete install for checking
install() ### runs R CMD INSTALL -> how to load? why in workflow?
# for compiled code use Build & Reload (ctrl + shift + B)


#### Miscellaneous ####
# ignore this file when building bundle or binary
use_build_ignore("dev.R")
